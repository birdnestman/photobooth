#!/usr/bin/python2.7
import sys, os
from Tkinter import *
import pifacedigitalio
from picamera import PiCamera
from time import sleep
import time
import pygame
from pygame.locals import *
from sys import exit


monitor_w = 800    # width of the display monitor
monitor_h = 480    # height of the display monitor

#############################
### Variables that Change ###
#############################
# Do not change these variables, as the code will change it anyway
transform_x = monitor_w  # how wide to scale the jpg when replaying
transfrom_y = monitor_h  # how high to scale the jpg when replaying
offset_x = 0  # how far off to left corner to display photos
offset_y = 0  # how far off to left corner to display photos


high_res_w = 1296  # width of high res image, if taken
high_res_h = 972  # height of high res image, if taken
high_res_w = 1296
high_res_h = 778
camera_iso = 500

pf = pifacedigitalio.PiFaceDigital()

pygame.init()
pygame.display.set_mode((monitor_w, monitor_h))
screen = pygame.display.get_surface()
pygame.display.set_caption('Photobooth')
pygame.mouse.set_visible(False)  # hide the mouse cursor
pygame.display.toggle_fullscreen()

screen.fill((0,0,0))


root_path = os.path.abspath(os.path.dirname(sys.argv[0]))

template_path = root_path + '/img/template-assets/'
img_path = root_path + '/Bilder/'
img_prefix = 'img_'

if not os.path.exists(img_path):
    os.makedirs(img_path)

# true if session runs
running_session = False

def getPath():
    stamp = time.strftime("%Y-%m-%d_%H%M%S")
    return img_path + img_prefix + stamp + '.jpg'

def buttonAction(event):
    pf = event.chip
    print "Button action: " + str(running_session)

    # cancel if session is running
    if running_session:
        return

    # big round button
    if event.pin_num == 5:
        startSession(pf)

    # centered silver button
    if event.pin_num == 6:
        exitSession(pf)

def startSession(pf):
    global running_session
    running_session = TRUE
    print "Session: " + str(running_session)

    pf.output_pins[1].turn_on()

    img_group = []
    pf.output_pins[4].turn_off()

    try:
        camera = PiCamera()
        camera.hflip = True
        camera.iso = camera_iso
        camera.resolution = (high_res_w, high_res_h)

        for x in range(1, 5):

            showImage(template_path + "image-" + str(x) + ".jpg")
            pf.output_pins[0].turn_on()
            sleep(1)
            pf.output_pins[2].turn_on()
            clear_screen()
            preview(2, camera)
            pf.output_pins[3].turn_on()
            clearScreenWhite()

            path = getPath()
            img_group.append(path)
            camera.capture(path)

            clear_screen()
            sleep(1)

            pf.output_pins[0].turn_off()
            pf.output_pins[2].turn_off()
            pf.output_pins[3].turn_off()


    finally:
        pf.output_pins[1].turn_on()
        camera.close()
        showFinalScreen()
        showImages(img_group)
        sleep(3)
        pf.output_pins[1].turn_off()
        showStartScreen(pf)

def preview(sec, camera):
    camera.start_preview()
    sleep(sec)
    camera.stop_preview()

# display a blank screen
def clear_screen():
    screen.fill((0, 0, 0))
    pygame.display.flip()

def clearScreenWhite():
    screen.fill((255, 255, 255))
    pygame.display.flip()

def showImages(images):
    for image in images:
        print(image)
        showImage(image)
        sleep(3)

def showImage(image_path):
    # clear the screen
    screen.fill((0, 0, 0))

    # load the image
    img = pygame.image.load(image_path)
    img = img.convert()

    # set pixel dimensions based on image
    set_demensions(img.get_width(), img.get_height())

    # rescale the image to fit the current display
    img = pygame.transform.scale(img, (transform_x, transform_y))
    screen.blit(img, (offset_x, offset_y))
    pygame.display.flip()

def showStartScreen(pf):
    # unlock session
    global running_session
    running_session = False

    clear_screen()
    showImage(template_path + "introduction.jpg")
    pf.output_pins[4].turn_on()

def showFinalScreen():
    clear_screen()
    showImage(template_path + "finish.jpg")


# set variables to properly display the image on screen at right ratio
def set_demensions(img_w, img_h):
    # Note this only works when in booting in desktop mode.
    # When running in terminal, the size is not correct (it displays small). Why?

    # connect to global vars
    global transform_y, transform_x, offset_y, offset_x

    # based on output screen resolution, calculate how to display
    ratio_h = (monitor_w * img_h) / img_w

    if (ratio_h < monitor_h):
        # Use horizontal black bars
        # print "horizontal black bars"
        transform_y = ratio_h
        transform_x = monitor_w
        offset_y = (monitor_h - ratio_h) / 2
        offset_x = 0
    elif (ratio_h > monitor_h):
        # Use vertical black bars
        # print "vertical black bars"
        transform_x = (monitor_h * img_w) / img_h
        transform_y = monitor_h
        offset_x = (monitor_w - transform_x) / 2
        offset_y = 0
    else:
        # No need for black bars as photo ratio equals screen ratio
        # print "no black bars"
        transform_x = monitor_w
        transform_y = monitor_h
        offset_y = offset_x = 0

def exitSession(pf):
    # if both buttons are pressed
    #if (event.chip.input_pins[5].value == 1 & pf.input_pins[6].value == 1):
    pf.output_port.all_off
    exit()


listener = pifacedigitalio.InputEventListener(chip=pf)
listener.register(5, pifacedigitalio.IODIR_FALLING_EDGE, buttonAction)
listener.register(6, pifacedigitalio.IODIR_FALLING_EDGE, buttonAction)
listener.activate()

pf.output_port.all_off

showStartScreen(pf)